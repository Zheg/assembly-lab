.386
.model flat, stdcall
option casemap:none
 
; ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤
    include \masm32\include\masm32rt.inc
; ¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤

    ; -------------------------------------------
    ; Build this DLL with the provided MAKEIT.BAT
    ; -------------------------------------------

    .data?
        hInstance dd ?

      .code

; «««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««

LibMain proc instance:DWORD,reason:DWORD,unused:DWORD 

    .if reason == DLL_PROCESS_ATTACH
      mrm hInstance, instance       ; copy local to global
      mov eax, TRUE                 ; return TRUE so DLL will start

    .elseif reason == DLL_PROCESS_DETACH

    .elseif reason == DLL_THREAD_ATTACH

    .elseif reason == DLL_THREAD_DETACH

    .endif

    ret

LibMain endp

; «««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««

SumFun proc first:DWORD, second:DWORD
    xor eax, eax
    mov eax, first
    add eax, second

    ret
SumFun endp

strLength proc source:dword
    push esi ;сохраним в стеке значения регистров esi, edi и ebx
    push edi ;прямо как настоящая stdcall-функция
    push ebx
 
    mov eax, source
    mov ecx, 0; это будет наш счетчик длины строки
    mov counter, ecx

    next_sym:
        mov ecx, counter
        mov bl, byte ptr [eax + ecx] ;загружаем очередной символ из строки в регистр bl
        test bl, bl ;проверяем, а не ноль ли он?
        jz end_count ;если ноль, идем на вывод длины
 
        ;если нет - считаем дальше
        inc ecx
        mov counter, ecx
        ;(эта команда эквивалентна add ecx, 1)
    jmp next_sym
 
    end_count:
 
    mov eax, counter ;помним, что все stdcall-функции возвращают значение в eax!
    ;мы ведь пишем, используя этот стандарт
 
 
    pop ebx
    pop edi ;восстановим значения регистров из стека
    pop esi ;в обратном порядке (помним принцип работы стека)
 
    ret ;выходим из процедуры

strLength endp


; На вход подаются ссылки на память
coppyString proc source:dword, dest:dword

	push esi
    push edi

	mov edi, dest
	lea esi, source
	
Lx:
	mov  al, [esi]
	mov  [edi], al
	inc  esi
	inc  edi
	cmp  al, 0 
	jne  Lx    

	pop edi
	pop esi

	ret
coppyString endp


; На вход подаются ссылки на память
stringProcessing proc source:dword, pattern:dword, dest:dword
    push esi 
    push edi

	invoke coppyString, source, dest ; Копирование изначальной строки в выходную переменную

lop:
    ; InString proc startpos:DWORD,lpSource:DWORD,lpPattern:DWORD
    ; Возвращает индекс начала вхождения подстроки lpPattern в lpSource
    ; 0 = нет вхождений 
    ; -1 = lpPattern длинее, чем lpSource 
    ; -2 = startpos больше, чем длина lpSource
    invoke  InString, 1, source, pattern 
    mov startPosition, eax
    cmp eax, 0
    jle exit_stringProcessing 	; Если нет вхождения подстроки, то выход
	
    invoke strLength, pattern	 
    mov patternLength, eax
	
    mov esi, source			
    mov edi, dest 
	
    dec startPosition 		; Нумерация в возвращённом значение начинается с 1, поэтому уменьшаем, чтобы привести к 0-индексации
    add esi, startPosition 	; Передвигаем указатели на начало подстроки
    add edi, startPosition 	; Передвигаем указатели на начало подстроки

    add esi, patternLength 	; Передвигаем укзатель вперёд на длину паттерна, 
                            ; таким образом сейчас он указывает на символ сразу за подстрокой, которую нужно исключить, 
                            ; и она не войдёт в выходное значние.
                            ; Функцией InString гарантировано, что startPosition + patternLength < sourceLength, 
                            ; Cледовательно указатель точно не выйдет за длину источника. 
	
    m:                      ; Копирование остатка строки
        mov al, [esi] 
        mov [edi], al
        inc esi
        inc edi
        cmp byte ptr [esi],0 
        jne m                
	
    invoke coppyString, dest, source 	; Копирование полученной строки в переменную источника 
    jmp lop                             ; Переход к поиску следующего вхождения подстроки
	
exit_stringProcessing:
    pop edi
    pop esi
    ret
stringProcessing endp
; «««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««««

end LibMain
